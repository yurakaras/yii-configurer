<?php

class Configurer {
    public $env = array();
    public $configList = array(
        "main.php",
        "default.php",
        "default_{\$env['TYPE']}.php",
        "{\$env['WORKSPACE']}/default.php",
        "{\$env['WORKSPACE']}/{\$env['TYPE']}.php",
    );

    protected $configBasePath;

    public function __construct($configBasePath) {
        $this->configBasePath = realpath($configBasePath);
    }

    protected function evaluatePath(){
        $env = $this->env;
        $env['TYPE']      = (PHP_SAPI === 'cli')?'console':'web';
        $env['HOSTNAME']  = trim(`hostname`);
        $env['VHOST']     = isset($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST']:$env['HOSTNAME'];
        $env['WORKSPACE'] = isset($_SERVER['YII_APP_WORKSPACE'])?$_SERVER['YII_APP_WORKSPACE']:$env['HOSTNAME'];

        $localList = array();
        foreach ($this->configList as $path) {            
            $path = $this->configBasePath.eval('return "/'.addcslashes($path, '"').'";');
            if (file_exists($path))
                $localList[] = $path;
        }
        return $localList;
    }

    public function formConfig(){
        if ($localList = $this->evaluatePath()) {
            foreach($localList AS $key => $value) $localList[$key] = require $value;
            return call_user_func_array('array_merge_recursive', $localList);
        }
        else {
            return array();
        }
    }

    public static function createInstance($configBasePath){
        return new static($configBasePath);
    }
}